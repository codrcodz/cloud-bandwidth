module github.com/nerdalert/cloud-bandwidth

go 1.12

require (
	github.com/sirupsen/logrus v1.8.1
	github.com/urfave/cli/v2 v2.3.0
	gopkg.in/yaml.v2 v2.4.0
)
